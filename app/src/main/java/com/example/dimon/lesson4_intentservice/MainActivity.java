package com.example.dimon.lesson4_intentservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    MyBroadcastReceiver receiver;
    final static String IMAGE_URL = "http://mirpozitiva.ru/uploads/posts/2016-08/1472042492_01.jpg";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.tvOperation);


    }

    public void onStartButtonClick(View v) {
        //Запускаем сервис, передавая ему новый Intent
        Intent intent = new Intent(this, FileDownloaderService.class);
        intent.putExtra("FILE_URL", IMAGE_URL);
        textView.setText("Download started");
        startService(intent);


        //Подписываемся на события нашего сервиса
        receiver = new MyBroadcastReceiver();
        IntentFilter intentFilter = new IntentFilter(
                FileDownloaderService.ACTION_MYINTENTSERVICE);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(receiver, intentFilter);
    }


    @Override
    protected void onStop() {
        //Отписываемся от событий сервиса
        unregisterReceiver(receiver);
        super.onStop();
    }


    public class MyBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String result = intent
                    .getStringExtra(FileDownloaderService.FINSHED_KEY_OUT);
            textView.setText(result);
        }
    }

}
