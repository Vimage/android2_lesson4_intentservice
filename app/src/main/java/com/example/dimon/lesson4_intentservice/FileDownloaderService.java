package com.example.dimon.lesson4_intentservice;


import android.app.IntentService;
import android.content.Intent;

import android.os.Environment;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Dimon on 10.01.2017.
 */

public class FileDownloaderService extends IntentService {

    public static final String ACTION_MYINTENTSERVICE = "com.exapmple.dimon.RESPONSE";
    public static final String FINSHED_KEY_OUT = "FINISHED";
    String finishedOutError = "ERROR Downloading file";


    public FileDownloaderService() {
        super("FileDownloaderService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {

        String fileURL = intent.getStringExtra("FILE_URL");

        long total = 0;
        int count;
        int lenghtOfFile = 0;

        try {
            URL url = new URL(fileURL);
            URLConnection conection = url.openConnection();
            conection.connect();


            lenghtOfFile = conection.getContentLength();


            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);


            OutputStream output = new FileOutputStream(Environment
                    .getExternalStorageDirectory().toString()
                    + "/new_name");

            byte data[] = new byte[1024];

            total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }


        Intent responseIntent = new Intent();
        responseIntent.setAction(ACTION_MYINTENTSERVICE);
        responseIntent.addCategory(Intent.CATEGORY_DEFAULT);
        if (total == lenghtOfFile) {
            responseIntent.putExtra(FINSHED_KEY_OUT, "File downloaded. This size = " + lenghtOfFile);
        } else {
            responseIntent.putExtra(FINSHED_KEY_OUT, finishedOutError);
        }

        sendBroadcast(responseIntent);

    }

}
